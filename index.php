<?php  
/*
GENERADO DESDE: http://picascii.com/

                              `+@#:`     `,'@#,                               
                            .@;  ;@@@@@@@@@@@'` `@'                            
                          +' ,@@@@@+:.````,'@@@@@' ,@                          
                        +: +@@@.               `+@@@``@                        
                      .+ @@@,                     `@@@`,+                      
                     @ '@@`                          @@@ @                     
                    @ @@:                             `@@,,:                   
                  .;.@@  `                              +@+ +                  
                 ..;#+                                   `@@ #                 
                .,+@:                                      @@ #                
               `,+@.    #@:,##                   ,@;,'@+    @@ #               
               ;;@.   +.:@@#@;,+                @`@@@@+ @:   @@ ;              
              @.@:   @  .:`:@@@ #              '@@@' ,, `,'   @@.`             
             @ @+   #.@#,`:@;@@@`;            @@@@ @`  ,@+ :   @:+             
            .`@@   +@,      ,#@@##           ;`@@ '       +.   ;@ @            
            +'@    '       ` '.@@ +          '@@.@       `` #   @@ `           
           + @:               @#@#@         ``@@,.               @:@           
           ;#@`               +`@@.#@@###@@@@@@#@                '@ ,          
          +`@``               +`@@;+@@@@@@@+:@@:@:                @;#          
          ++@              `@ '@@@@@@@@@@@@@@@@@+ :#              '@ `         
         . @,             @ @@@@@@.  `.,..`  @@#@@@:.'             @`@         
         @,@            .;#+``';@@ :       .+@@ @' `#.#            @@;         
         ;@@           ,:`#@:  '@@ `        +@@ . .;@#.;           .@ .        
        ` @.           @'      '@@ `        +@@ .      :@           @`#        
        ' @           `        '@@ `        #@@ .                   @+@        
        @;@                    '@@ ;,`     .;+@',                   +@:        
        ##@                ,@, ;@@ @@@@@@@@@@@#: ,@.                ,@         
        ;@+              ':.@@@,@@ @@@#++++#@@@@@@# ::               @ .       
        .@:            `+:@@@@;.@@ #@@#+';''@@@', `,#.;              @ ;       
         @.            '@@@;.@+`@@          #@@:;  .'@'`'            @ '       
         @`           ;;@@:@   `@@          #@@;+       `'           @`+       
       ` @`           @@@@:@    @@` `;+@##+'@@@+@#,                  @`#       
         @`           +;@@@' .;':,` `;#@@@@@@@@#@#, ;,               @`+       
         @`            @'@@@@@@@@@@@@@@@@@@@@#@@#@@@@,'              @ +       
        `@:             @`;@@@@@@@@@@@#;.   ,+@@+ .#@@@,             @ ;       
        ,@'               ;@'.  ``,;#@#;.`  `:@@, .@`@@;@            @ ,       
        +@@         +         +@@+#          `@@    @'@@#           .@         
        @'@          ;;;      +@@:'          `@@ ` + @@#@           '@.        
        #`@           #` .++,``@@ .          '@@'@:,@@# :           @##        
        . @`            # @#;..@@ @@@+';;'+@@+, .#@@@@ +            @,@        
         ,@+              #.,@#@@;@@+;::::'#@@@@@@@@ #`            `@ :        
         @'@                .+@@#@@@@@@@@@@@@@@@#`,@`              #@.         
         ; @`                 @@.@+:`  `..`   :#@''                @:@         
          ,@@               @#@@`      ````   @@@@.               ,@ ,         
          @`@                @@#@             ``@@:#              @#'          
           `@@            `#@@@ ,              #@@@ .            .@ +          
           @`@  @:       `+##@;@               ` @@@ :       `:` @+;           
            ,@@  ' '#:,+@ @@@#+                 +`@@@ '#;;#@. . +@ ;           
            + @+ .#;#:,'@@@@:+.                  @ @@@@+::+@.` `@.@            
             @.@.  @,,@@@@; @                     ;',@@@@'`@   @@,             
              :+@  ``@@##@#  `                      '@##@+ `  @@ '             
              ,`@@                                           @@ @              
               ' @@                                         @@ @               
                # @@                                       @@ @                
                 # #@`                                    @@ @ `               
                  +`@@;`                                `@@ @                  
                   :,:@@                               +@# @                   
                     # @#'                           ,@@.,;                    
                      @`:@@'                       ,@@+ @ `                    
                       `@ ;@@@.                 `#@@# ;;                       
                         .@ .@@@@+.         .'@@#@; '+                         
                           `@; `+@@@@@@@@@@@@@#, .@,                           
                              `#@:    `.``   .#@:                              
                                   `:'+##+;.                          

*/
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>hIvE</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<style>
		.draggable { width: 100px; height: 100px; padding: 0px; float: left; margin: 0px; top: 30px; left: 5px; position: absolute;}
		.ui-widget-header p, .ui-widget-content p { margin: 0; }
		
    #snaptarget { 
      height: 540px; 
      border: 1px solid black;
    }

    #piezas { 
      height: 120px; 
      border: 1px solid black;
      margin-bottom: 10px;
    }

    .hexagono { 
       width: 100px; 
       height: 55px; 
       background: #000000; 
       position: relative; } 

    .hexagono:before { 
       width: 0; 
       height: 0; 
       content: ""; 
       position: absolute; 
       top: -25px; 
       left: 0; 
       border-left: 50px solid transparent; 
       border-right: 50px solid transparent; 
       border-bottom: 25px solid #000000;
    } 

    .hexagono:after { 
       width: 0; 
       height: 0; 
       content: ""; 
       position: absolute; 
       bottom: -25px; 
       left: 0; 
       border-left: 50px solid transparent; 
       border-right: 50px solid transparent; 
       border-top: 25px solid #000000;
    }

	</style>
	
  </head>
  <body>
    
    <section class="container">

      <header>
         <h1>Implementacion de hIvE - <small>https://jqueryui.com/draggable/#constrain-movement</small></h1>
      </header>

		  <hr>
      
      <section id="piezas">

        <div class="hexagono draggable ui-widget-content"></div>
        <div class="hexagono draggable ui-widget-content"></div>
        <div class="hexagono draggable ui-widget-content"></div>
        <div class="hexagono draggable ui-widget-content"></div>
        <div class="hexagono draggable ui-widget-content"></div>
        <div class="hexagono draggable ui-widget-content"></div>
      
      </section>

      <section id="snaptarget">

      
      </section>


      <hr>
      <footer>
         <p id="txt">Coordenadas</p>
      </footer>

    </section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.2.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Para jquery UI -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
		$( function() {

      //Le digo el tamaño del grid donde lo anclara
  		$( ".draggable" ).draggable({ grid: [ 10,  10] });
      
      //Le digo que al soltar el hexagono, me diga su posicion
      $( ".draggable" ).mouseup(function(){
        var coordenada='X:' + $(this).position().left + ', Y:' + $(this).position().top;
        $("#txt").html(coordenada);
      });

		} );
	</script>

  </body>
</html>